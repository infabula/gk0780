document.addEventListener("DOMContentLoaded", onPageLoad);
var participants = undefined;

function createParticipants(){
    let participantArray = new Array();
    let id = 101;

    let ellen = new Participant(id, "silver", "Ellen", "Elsinga");
    participantArray.push(ellen);
    id += 1;
    let bob = new Participant(id, 'bronze', "Bob", "Bouwer");
    participantArray.push(bob);
    
    participantArray.push(new Participant(++id, 'bronze', "Freek", "Vriend"));
    participantArray.push(new Participant(++id, 'gold', "Uwe", "Kort"));
    participantArray.push(new Participant(++id, 'server', "Stefan", "Wenders"));
    
    return participantArray;
}


function letThemGreet(memberArray){
    memberArray.forEach(function(participant, index, array){
        participant.greet();
    });

    /*
    for(let index in memberArray){
        let participant = memberArray[index];
        participant.greet();
    }
    */
}


function appendParticipantToDOMElement(participant, element_id){
    let parentElement = document.getElementById(element_id);
    let childElement = document.createElement('li');
    childElement.innerHTML = participant.firstName + " " + participant.lastName;
    parentElement.append(childElement); // hang in de DOM
}

function showAsHtml(memberArray, element_id){
    // eerst alle kinderen van element_id opruimen
    document.getElementById(element_id).innerHTML = '';

    memberArray.forEach(function(participant, index, array){
        appendParticipantToDOMElement(participant, element_id);
    });
}

function filterByMembertype(participants, filterType){
    let filteredParticipants = participants.filter( 
        (participant, index, array) => {
            if (participant.memberType == filterType){
                return participant;
            }
        });
    return filteredParticipants;
}

function updateFilterResults(evt){
    console.log("Filter is gewijzigd.");
    let filterType = evt.target.value;
    console.log("filter op", filterType);

    let filtered = filterByMembertype(participants, filterType);
    console.log(JSON.stringify(filtered));
    showAsHtml(filtered, 'filterList');
}

// begin van code uitvoeren
function onPageLoad(){
    // callbacks registreren
    let filterElement = document.getElementById('filter');
    filterElement.addEventListener('change', updateFilterResults)

    participants = createParticipants();

    // make json representation
    let participantAsJSON = JSON.stringify(participants);
    console.log("JSON:", participantAsJSON);

    letThemGreet(participants);
    showAsHtml(participants, 'participantList');


    let filterType = "bronze";
    let filtered = filterByMembertype(participants, filterType);
   
    console.log(JSON.stringify(filtered));
    showAsHtml(filtered, 'filterList');

}





