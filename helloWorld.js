document.addEventListener("DOMContentLoaded", onPageLoad);

function onPageLoad(){
    let theButton = document.getElementById("aButton");
    theButton.addEventListener("click", handleButtonClick);
    theButton.addEventListener("mouseover", mouseOverEvent);
    theButton.addEventListener("mouseout", mouseOutEvent);

    let nameInput = document.getElementById("nameInput");
    nameInput.addEventListener('change', nameChanged);
    nameInput.addEventListener("mouseover", mouseOverEvent);
    console.log("Geregistreerd.")
}

function nameChanged(evt){
    console.log(evt.target);
    console.log("Naamveld is gewijzigd.");
    let heading = document.getElementById("head1");
    //let nameInput = document.getElementById("nameInput");
    //let name = nameInput.value;
    let name = evt.target.value;
    let message = "hello " + name + "!";
    heading.innerHTML = message;
}

function mouseOverEvent(evt){
    let id = evt.target.id;
    console.log("mouseOver on " + id);
}

function mouseOutEvent(evt){
    console.log("mouseOut");
}

function handleButtonClick(evt){
    console.log(evt.target);
    console.log("button was clicked");
    let heading = document.getElementById("head1");
    heading.innerHTML = "Button geklikt";

    //let theButton = document.getElementById("aButton");
    //theButton.removeEventListener("click", handleButtonClick);
}

