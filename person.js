class Person{
    constructor(firstName, lastName){
        console.log("Constructor aangeroepen.") 
        this.firstName = firstName;  // property
        this.lastName = lastName;    // property
        this.age = undefined;        // property
    }

    greet(){  // member function
        console.log("Hello, my name is", this.firstName);
    }
}
/*
let bob = new Person("Bob", "Builder");
let eve = new Person("Eve", "Evens");

console.log(bob.firstName)
console.log(eve.firstName);

bob.greet();
eve.greet();
*/



