class Participant extends Person {
    constructor(id, memberType, firstName, lastName){
        super(firstName, lastName);
        this.id = id;
        this.memberType = memberType;
    }
}