
function print_circle_properties(circle_obj){
    console.log("Radius", circle_obj.radius);
    console.log("Color", circle_obj['color']);
    console.log("Area", circle_obj.area());
    return undefined; 
}

var circle = {
    'radius': 5.6,  // property, data
    'color': 'red', // property, data
    'area': function(){  // functionaly, member
         let oppervlak = this.radius * this.radius * Math.PI;
         return oppervlak;
    }
}

var circle2 = {
    'radius': 2.3,  // property, data
    'color': 'green', // property, data
    'area': function(){  // functionaly, member
         let oppervlak = this.radius * this.radius * Math.PI;
         return oppervlak;
    }
}


circle.radius *= 2;

let function_result = print_circle_properties(circle);
console.log("De functie retourneert", function_result);

print_circle_properties(circle2);


/*
console.log(circle.radius);
circle.radius *= 2;

console.log(circle['radius']);

let property = 'radius';
console.log(circle[property]);

let surface_area = circle.area();
console.log("Oppervlak", surface_area);
*/








