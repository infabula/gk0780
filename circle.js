class Circle{
    constructor(radius, color){
        this.radius = radius;
        this.color = color;
    }

    area(){
        return this.radius * this.radius * Math.PI;
    }
}

let red_circle = new Circle(3.0, "red");
let green_circle = new Circle(4.0, "green");

console.log("Red circle area", red_circle.area())
console.log("Gree circle area", green_circle.area())
